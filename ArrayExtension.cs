﻿using System.Collections.Generic;
using System.Linq;

namespace MultithreadedArraySum
{
    public static class ArrayExtension
    {
        /// <summary>
        /// Разбивает массив на несколько массивов.
        /// </summary>
        /// <typeparam name="T">Тип массива.</typeparam>
        /// <param name="array">Массив для разделения.</param>
        /// <param name="size">Размерность маленьких массивов.</param>
        /// <returns>Возвращает список массивов.</returns>
        public static IEnumerable<T[]> Split<T>(this T[] array, int size)
        {
            for (var i = 0; i < (float)array.Length / size; i++)
            {
                yield return array.Skip(i * size).Take(size).ToArray();
            }
        }
    }
}
