﻿using Microsoft.Extensions.Configuration;

using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace MultithreadedArraySum
{
    /// <summary>
    /// Класс для расчета суммы элементов массива параллельно.
    /// </summary>
    public static class Array
    {
        /// <summary>
        /// Массив элементов типа int
        /// </summary>
        private static int[] _array;

        /// <summary>
        /// Максимальное значение элементов массива.
        /// </summary>
        private static int _arrayMaxValue;

        /// <summary>
        /// Количество элементов в массиве.
        /// </summary>
        private static int _arraySize;

        /// <summary>
        /// Сумма элементов массива.
        /// </summary>
        private static long _arraySum;

        /// <summary>
        /// Конфигурация, для получения параметров из json.
        /// </summary>
        private static IConfiguration _configuration;


        private static readonly Stopwatch _stopwatchSer = new Stopwatch();

        /// <summary>
        /// Количество потоков.
        /// </summary>
        private static int _threadCount;

        /// <summary>
        /// Рассчитывает сумму массива без распараллеливания.
        /// </summary>
        private static void ArraySum()
        {
            Console.WriteLine("Обычный расчет суммы массива.");

            _stopwatchSer.Restart();

            _arraySum = _array.Sum();

            _stopwatchSer.Stop();

            Console.WriteLine($"Сумма массива: {_arraySum}. " +
                $"Время расчета: {_stopwatchSer.ElapsedMilliseconds}");
        }

        /// <summary>
        /// Генерирует массив случайнах значений.
        /// </summary>
        /// <returns></returns>
        private static int[] CreateArray()
        {
            var rand = new Random();

            var arr = new int[_arraySize];

            for (int i = 0; i < _arraySize; i++)
            {
                arr[i] = rand.Next(_arrayMaxValue);
            }

            return arr;
        }

        /// <summary>
        /// Считывает параметры из конфигурации.
        /// </summary>
        /// <param name="nameParam">Строковое наименование параметра.</param>
        /// <returns>Возвращает значение параметра.</returns>
        private static int GetParamConfig(string nameParam)
        {
            // Получаем строковое значение параметра.
            var countArraystring = _configuration[nameParam];

            if (!int.TryParse(countArraystring, out int valueParam))
            {
                Console.WriteLine("Данные конфигурации должны быть целочисленными.");

                return 0;
            }

            return valueParam;
        }

        /// <summary>
        /// Рассчитывает сумму элементов массива с помощью Parallel LINQ.
        /// </summary>
        private static void ParallelLinqArraySum()
        {
            Console.WriteLine("Параллельный расчет суммы массива с помощью Linq.");

            _stopwatchSer.Restart();

            var arraySum = _array
                .AsParallel()
                .Sum();

            _stopwatchSer.Stop();

            Console.WriteLine($"Сумма массива: {arraySum}. " +
                $"Время расчета: {_stopwatchSer.ElapsedMilliseconds}");
        }
        
        /// <summary>
        /// Запуск методов расчета суммы элементов массива.
        /// </summary>
        /// <param name="configuration">Конфигурация с парсметрами.</param>
        public static void Start(IConfiguration configuration)
        {
            _configuration = configuration;
            
            if (_configuration == null)
            {
                throw new ArgumentNullException(nameof(_configuration));
            }

            _arraySize = GetParamConfig("arraySize");

            _threadCount = GetParamConfig("threadCount");

            _arrayMaxValue = GetParamConfig("arrayMaxValue");

            if (_threadCount == 0 || _arraySize == 0 || _arrayMaxValue == 0)
            {
                Console.WriteLine("Некорректные данные в конфигурации.");
                return;
            }

            // Создание массива.
            _array = CreateArray();

            Console.WriteLine($"Создан массив размером в {_arraySize} элементов.\n");

            // Рассчитываем сумму массива без распараллеливания.
            ArraySum();
            Console.WriteLine();

            // Рассчитываем сумму массива с помощью потоков.
            ThreadArraySum();
            Console.WriteLine();

            // Рассчитываем сумму массива с помощью потоков.
            ParallelLinqArraySum();
        }

        /// <summary>
        /// Рассчитывает сумму элементов массива с помощью потоков.
        /// </summary>
        private static void ThreadArraySum()
        {
            Console.WriteLine("Параллельный расчет суммы массива с помощью потоков.");

            _arraySum = 0;

            // Рассчитываем количество элементов для массивов.
            var arrayCount = Math.Ceiling((double)_array.Length / _threadCount);

            // Получаем список массивов.
            var arraySplit = _array.Split(Convert.ToInt32(arrayCount));

            var waitHandles = new WaitHandle[_threadCount];

            int i = 0;

            _stopwatchSer.Restart();

            object locker = new object();

            foreach (var item in arraySplit)
            {
                var handle = new EventWaitHandle(false, EventResetMode.ManualReset);

                var thread = new Thread(() =>
                    {
                        lock (locker)
                        {
                            _arraySum += item.Sum();
                        }

                        handle.Set();
                    });

                waitHandles[i] = handle;

                i++;

                thread.Start();
            }

            WaitHandle.WaitAll(waitHandles);

            _stopwatchSer.Stop();

            Console.WriteLine($"Сумма массива: {_arraySum}. " +
                $"Время расчета: {_stopwatchSer.ElapsedMilliseconds}");
        }
    }
}
