﻿using Microsoft.Extensions.Configuration;

namespace MultithreadedArraySum
{
    class Program
    {

        static void Main(string[] args)
        {
            // подключение файла конфигурации
            IConfiguration configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();

            Array.Start(configuration);
        }

        
    }
}
